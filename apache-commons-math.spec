%global short_name commons-math3
Name:                apache-commons-math
Version:             3.6.1
Release:             3
Summary:             Java library of lightweight mathematics and statistics components
License:             Apache-1.1 and Apache-2.0 and BSD-3-Clause
URL:                 http://commons.apache.org/math/
Source0:             http://archive.apache.org/dist/commons/math/source/%{short_name}-%{version}-src.tar.gz
BuildArch:           noarch
BuildRequires:       jpackage-utils maven-local
BuildRequires:       mvn(org.apache.commons:commons-parent:pom:) mvn(junit:junit)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
Requires:            jpackage-utils
%description
Commons Math is a library of lightweight, self-contained mathematics and
statistics components addressing the most common problems not available in the
Java programming language or Commons Lang.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%autosetup -n %{short_name}-%{version}-src -p1
sed -i -e '/checkMissingFastMathClasses/i@Ignore' \
src/test/java/org/apache/commons/math3/util/FastMathTest.java
%if "%{_arch}" == "riscv64"
sed -i -E 's/timeout=20000L/timeout=200000L/' src/test/java/org/apache/commons/math3/util/FastMathTest.java
%endif
%mvn_alias "org.apache.commons:%{short_name}" "%{short_name}:%{short_name}"
%mvn_file :%{short_name} %{short_name} %{name}

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export PATH=$JAVA_HOME/bin:$PATH
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build -- -Dmaven.compiler.source=11 -Dmaven.compiler.target=11

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt NOTICE.txt RELEASE-NOTES.txt

%files javadoc -f .mfiles-javadoc
%doc LICENSE.txt NOTICE.txt

%changelog
* Wed Oct 16 2024 pSomng <pisong.oerv@isrc.iscas.ac.cn> - 3.6.1-3
- set openjdk11 for build

* Tue Apr 09 2024 Dingli Zhang <dingli@iscas.ac.cn> - 3.6.1-2
- Change timeout for riscv64

* Wed Jul 20 2022 liyanan <liyanan32@h-partners.com> - 3.6.1-1
- Update to 3.6.1

* Mon May 09 2022 chenchen <chen_aka_jan@163.com> - 3.4.1-2
- License compliance rectification

* Tue Jul 28 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 3.4.1-1
- Package init
